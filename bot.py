#!/usr/bin/env python3
"""
A discord bot to provide wardrobe services to a torei related server.

Stores a list of outfits, and can be used to dress users in their choice of outfit, or something
randomly selected from the list.
"""

import sys
import os
import re
import random
import asyncio
import logging
import typing
import yaml
import discord
from discord.ext import commands
from datetime import date

# Since we're not using voice, disable the warning about the dependencies not being installed
discord.VoiceClient.warn_nacl = False

CONFIG_DIR = os.path.dirname(os.path.abspath(__file__))


logging.basicConfig(level=logging.WARN)

try:
    with open(os.path.join(CONFIG_DIR, "secrets.yaml")) as config_file:
        config = yaml.safe_load(config_file)
except FileNotFoundError:
    # No config file exists, but that's okay
    config = {}

if "auth_token" not in config or "client_id" not in config:
    print("Auth token or client id not found")
    sys.exit()

print("Invite link: ", end="")
print(discord.utils.oauth_url(
    config['client_id'], # Bot ID (not a secret)
    discord.Permissions(read_messages=True, send_messages=True),
))


outfits = None
try:
    with open(os.path.join(CONFIG_DIR, "outfits.yaml")) as outfit_file:
        outfits = yaml.safe_load(outfit_file)
except FileNotFoundError:
    # No config file exists, but that's okay
    pass

if outfits is None:
    outfits = {}


def is_april_fools():
    """Check if today is April fool's day."""
    return date.today().month == 4 and date.today().day == 1


bot = commands.Bot(
    command_prefix = commands.when_mentioned,
    case_insensitive = True,
    activity = discord.Game("with laminate"),
)


@bot.event
async def on_command_error(ctx, error):
    """Produce an error message on an unrecognised command."""
    if isinstance(error, commands.CommandNotFound):
        await ctx.channel.send(f"{ctx.author.mention} I didn't recognise that request")
        return

    await ctx.channel.send(f"{ctx.author.mention} `ERROR CODE {random.randrange(256,512)}`")
    raise error


@bot.event
async def on_ready():
    """Let the debug log know when the bot has fully started."""
    for guild in bot.guilds:
        for role in guild.roles:
            if role.name == "Master":
                bot.minimum_role_for_dressing_others = role
                break
        print(f"Connected as {bot.user.display_name} to {guild.name} with minimum role of "\
              f"{bot.minimum_role_for_dressing_others.name}")


@bot.command(usage="<me|name of person> [in|with <outfit name>]")
async def dress(ctx, target : typing.Optional[discord.Member], *args):
    """
    Dress someone in a specified outfit, or let the wardrobe pick one.

    If the outfit name is not specified, the wardrobe will select one at random.

    Examples:
        `@wardrobe dress me in catsuit`
        `@wardrobe dress me` (for a random selection)
        `@wardrobe dress @other_person in catsuit`
        `@wardrobe dress @other_person (for a random selection)
    """
    requested_outfit = " ".join(args)
    if requested_outfit.startswith("me"):
        requested_outfit = requested_outfit[3::].strip()
        target = ctx.author
    if re.match(r"^(with|in) ", requested_outfit):
        requested_outfit = requested_outfit.split(" ",1)[-1]
    if not requested_outfit:
        requested_outfit = random.choice(list(outfits))

    # Check the requester has permission to dress the target
    if target != ctx.author and ctx.author.top_role < bot.minimum_role_for_dressing_others:
        await ctx.channel.send(
            f"{ctx.author.mention} does not have permission to redress {target.display_name}!"
        )
        return

    if requested_outfit not in outfits:
        await ctx.channel.send(
            f"{ctx.author.mention} I don't seem to be programmed for that outfit."
        )
        return

    async with ctx.channel.typing():
        await asyncio.sleep(1)
        if not target:
            await ctx.channel.send(
                f"Not sure who you wanted to dress {ctx.author.mention}, please retry"
            )
            return

        if is_april_fools():
            requested_outfit = "strapped bag"

        if "equip_message" not in outfits[requested_outfit]:
            if target == ctx.author:
                message = f"{ctx.author.mention} enters the wardrobe and returns dressed in "\
                          f"{outfits[requested_outfit]['description']}"
            else:
                message = f"{ctx.author.mention} sends {target.mention} into the wardrobe and "\
                          f"they return dressed in {outfits[requested_outfit]['description']}"
        else:
            message = outfits[requested_outfit]["equip_message"]
            if target == ctx.author:
                message = message.replace("$TARGET", ctx.author.mention)
            else:
                message = message.replace("$TARGET", target.mention)

        embed = None
        if "urls" in outfits[requested_outfit]:
            embed = discord.Embed().set_image(url=random.choice(outfits[requested_outfit]["urls"]))

        await ctx.channel.send(message, embed=embed)
        print(f"Dressing {target.display_name} in '{requested_outfit}' outfit")


@bot.command(name = "list", usage="[tag[,tag,...]]")
async def list_command(ctx, args: typing.Optional[str] = ""):
    """List the available outfits, optionally filtering by tag."""
    chunk = ""
    msg_chunks = [""]
    tags = args.split(",")
    for name in outfits:
        if not args or 'tags' in outfits[name] and set(tags).issubset(set(outfits[name]['tags'])):
            new_chunk = f"**{name.title()}**\n"
            new_chunk += f"{outfits[name]['description']}\n\n"
            if len(''.join(msg_chunks) + new_chunk) > 2000:
                msg_chunks.append(new_chunk)
            else:
                msg_chunks[-1] += new_chunk

    if not msg_chunks[0]:
        msg_chunks = ["No outfits defined matching those criteria."]

    for chunk in msg_chunks:
        await ctx.channel.send(chunk)


@bot.group(name="add", invoke_without_command=True)
async def cmd_add(ctx):
    """Commands for adding new outfits."""
    if ctx.subcommand_passed is not None:
        await ctx.send(f"{ctx.author.mention} Subcommand {ctx.subcommand_passed} not recognised")
    else:
        await ctx.send(f"{ctx.author.mention} Add what?")


@cmd_add.command(name="outfit", usage="<outfit name> = <outfit description>")
async def subcmd_outfit(ctx, *, args: typing.Optional[str]):
    """
    Add a new outfit to the wardrobe.

    Both a name and description must be specified, separated by '='.

    Example:
        `@wardrobe add outfit bikini = some kind of a bikini.`
    """
    if not args:
        await ctx.channel.send(f"{ctx.author.mention} You'll have to tell me what to add!")
        return
    try:
        (name, description) = args.split("=")
        name = name.strip().lower()
        description = description.strip()
    except ValueError:
        await ctx.channel.send(f"{ctx.author.mention} An outfit requires a name and description, "\
                                "as in `add outfit catsuit = A shiny black catsuit`.")
        return

    if name in outfits:
        await ctx.channel.send(f"{ctx.author.mention} An outfit with that name already exists.")
        return

    outfits[name] = { "description": description }
    print(f"Created outfit '{name}'")

    with open(os.path.join(CONFIG_DIR, "outfits.yaml"), mode='w') as file_handle:
        print(yaml.dump(outfits), file=file_handle, end='')

        await ctx.channel.send(f"{ctx.author.mention} New outfit saved.")
        return


@cmd_add.command(name="image", usage="<outfit name> <pasted image>")
async def subcmd_image(ctx, *, name: typing.Optional[str]):
    """
    Add an image to an existing outfit.

    The outfit must already exist in the database, and the image
    should be pasted into the message, not sent as a URL.
    """
    if name and name.startswith("to"):
        name = name[3::].strip()
    if not name:
        await ctx.channel.send(
            f"{ctx.author.mention} You'll have to tell me what outfit to add to!"
        )
        return

    if name not in outfits:
        await ctx.channel.send(
            f"{ctx.author.mention} I don't seem to be programmed for that outfit."
        )
        return

    urls = []
    for attachment in ctx.message.attachments:
        # To check for a valid image/video that discord can embed
        if attachment.url.startswith('https://') and attachment.height is not None:
            urls.append(attachment.url)
            print(f"Adding an image to '{name}' outfit")

    if 'urls' in outfits[name]:
        outfits[name]['urls'] += urls
    else:
        outfits[name]['urls'] = urls

    with open(os.path.join(CONFIG_DIR, "outfits.yaml"), mode='w') as file_handle:
        print(yaml.dump(outfits), file=file_handle, end='')

        await ctx.channel.send(f"{ctx.author.mention} Image added.")
        return


@cmd_add.command(name="equip", usage="<outfit name> = <equip message>")
async def subcmd_equip(ctx, *, args: typing.Optional[str]):
    """
    Add a description of what happens when this outfit is worn.

    By default, the wardrobe produces messages along the lines
    of "@user enters the wardrobe and returns wearing <outfit
    description>". This option allows you to replace this
    message with a more descriptive version of what happens
    during the process. The outfit must already exist in the
    database. Include '$TARGET' to mention the person being
    dressed's name.

    Example:
        `@wardrobe add equip naked = As $TARGET steps inside the
         wardrobe, the machine's arms grab them and dissolve all
         of their clothes!`
    """
    if not args:
        await ctx.channel.send(f"{ctx.author.mention} You'll have to tell me what to add!")
        return
    try:
        (name, message) = args.split("=")
        name = name.strip().lower()
        message = message.strip()
    except ValueError:
        await ctx.channel.send(f"{ctx.author.mention} This requires a name and equip message, "\
                                "as in `add equip catsuit = enters the wardrobe and... `"\
                                "`$TARGET` will be replaced with the target's name.")
        return

    if name not in outfits:
        await ctx.channel.send(
            f"{ctx.author.mention} I don't seem to be programmed for that outfit."
        )
        return

    outfits[name]['equip_message'] = message
    print(f"Added equip message to outfit '{name}'")

    with open(os.path.join(CONFIG_DIR, "outfits.yaml"), mode='w') as file_handle:
        print(yaml.dump(outfits), file=file_handle, end='')

        await ctx.channel.send(f"{ctx.author.mention} Equip message saved.")
        return


@cmd_add.command(name="tag", usage="<outfit name> = <tag>[,<tag>,...]")
async def subcmd_tag(ctx, *, args: typing.Optional[str]):
    """
    Add a grouping tag to an outfit.

    Tags are used for organising outfits into groups.

    Example:
        `@wardrobe add tag catsuit = tight,lockable`
    """
    if not args:
        await ctx.channel.send(f"{ctx.author.mention} You'll have to tell me what to add!")
        return
    try:
        (name, tag_str) = args.split("=")
        name = name.strip().lower()
        tags = []
        for tag in tag_str.split(','):
            tags.append(tag.strip())
    except ValueError:
        await ctx.channel.send(f"{ctx.author.mention} This requires an outfit name and tag(s) to "\
                                " add, as in `add tag catsuit=tight,lockable`")
        return

    if name not in outfits:
        await ctx.channel.send(
            f"{ctx.author.mention} I don't seem to be programmed for that outfit."
        )
        return

    if 'tags' in outfits[name]:
        outfits[name]['tags'] += tags
    else:
        outfits[name]['tags'] = tags
    outfits[name]['tags'] = list(dict.fromkeys(outfits[name]['tags'])) # De-duplicate the tags
    print(f"Added {', '.join(tags)} tags to outfit '{name}'")

    with open(os.path.join(CONFIG_DIR, "outfits.yaml"), mode='w') as file_handle:
        print(yaml.dump(outfits), file=file_handle, end='')

        await ctx.channel.send(f"{ctx.author.mention} **{', '.join(tags)}** tags added to {name}.")
        return


bot.run(config['auth_token'])
